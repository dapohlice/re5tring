#!/bin/bash

if [ -z "$1" ]
  then
    echo "No install path given"
    exit 1
fi

echo 'Install re5tring'

bin_path=$1

mkdir -p $bin_path

echo 'Copy re5tring.jar to {$bin_path}' 

cp re5tring.jar $bin_path/re5tring.jar

path_string=$PATH:$bin_path 

export PATH=$path_string


echo 'export re5tring place to PATH '

echo 'export PATH=$PATH:'$bin_path >> ~/.profile

echo 'Create Shell Scripts'

echo '#!/bin/bash' > $bin_path/r5
echo '#!/bin/bash' > $bin_path/r5create
echo '#!/bin/bash' > $bin_path/r5var

echo 'java -cp '$bin_path'/re5tring.jar code.pohli.re5tring.Launcher $@' >> $bin_path/r5
echo 'java -cp '$bin_path'/re5tring.jar code.pohli.re5tring.SettingsManager $@' >> $bin_path/r5create
echo 'java -cp '$bin_path'/re5tring.jar code.pohli.re5tring.VariableSettingsLauncher $@' >> $bin_path/r5var

echo 'Add execute rights to Scripts'

/usr/bin/chmod +x $bin_path/r5
/usr/bin/chmod +x $bin_path/r5create
/usr/bin/chmod +x $bin_path/r5var

echo 'Install finished'

