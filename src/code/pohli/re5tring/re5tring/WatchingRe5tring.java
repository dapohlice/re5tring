package code.pohli.re5tring.re5tring;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import code.pohli.utils.console.Console;
import code.pohli.fileObserver.FileObserver;
import code.pohli.re5tring.par5er.DelayedPar5er;
import code.pohli.re5tring.par5er.FilePar5er;
import code.pohli.re5tring.placeholder.PList;
import code.pohli.re5tring.placeholder.PlaceholderObserver;
import code.pohli.re5tring.settings.General;
import code.pohli.re5tring.walker.DeletingWalker;
import code.pohli.re5tring.walker.SimpleCodeWalker;

public class WatchingRe5tring implements Re5tring{

	private Path start,dest;
	
	private DelayedPar5er delayedParser;
	private FileObserver sourceFileObserver;
	
	private FileObserver placeholderFileObserver;
	private PlaceholderObserver  placeholderObserver;
	
	private PList placeholderList;
	
	
	public WatchingRe5tring() {
		
		Path placeholderPath = General.getSettings().getPlaceholderPath();
		
		placeholderList = new PList(placeholderPath);
		placeholderList.load(true);
		
		this.dest = General.getSettings().getDestinationPath();
		this.start = General.getSettings().getSourcePath();
		
		
		
		FilePar5er fp = new FilePar5er(placeholderList, this.dest, this.start);
		
		this.delayedParser = new DelayedPar5er(fp);	
		this.sourceFileObserver = new FileObserver(delayedParser, start);
		
		SimpleCodeWalker codeWalker = new SimpleCodeWalker(placeholderList, dest, start);
		
		this.placeholderObserver = new PlaceholderObserver(placeholderList, codeWalker, placeholderPath, true);
		this.placeholderFileObserver = new FileObserver(placeholderObserver, placeholderPath); 

		Console.log("Observing "+start.toString());
	}

	@Override
	public boolean start() {
		
		
		try {
			Files.walkFileTree(this.dest, new DeletingWalker(this.dest));
		} catch (IOException e1) {
			Console.error(String.format("Destination cleanup %s failed",this.dest.toAbsolutePath().toString()));
		}

		boolean sourceSucced = this.sourceFileObserver.start();
		boolean placeholderSucced = this.placeholderFileObserver.start();
		
		if(sourceSucced && placeholderSucced)
			return true;
		
		stop();
		return false;
	}

	@Override
	public void stop() {
		 this.sourceFileObserver.stop();	
		 this.placeholderFileObserver.stop();
	}
}
