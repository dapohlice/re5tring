package code.pohli.re5tring.re5tring;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import code.pohli.utils.console.Console;
import code.pohli.re5tring.placeholder.PList;
import code.pohli.re5tring.settings.General;
import code.pohli.re5tring.walker.DeletingWalker;
import code.pohli.re5tring.walker.SimpleCodeWalker;

public class SimpleRe5tring implements Re5tring{
	
	private PList plist;
	
	private SimpleCodeWalker simpleCodeWalker;
	
	private Path dest;
	private Path start;
	
	public SimpleRe5tring() {
		Path placeholderPath = General.getSettings().getPlaceholderPath();
		
		plist = new PList(placeholderPath);
		plist.load(true);
		
		dest = General.getSettings().getDestinationPath();
		
		start = General.getSettings().getSourcePath();
		
		simpleCodeWalker = new SimpleCodeWalker(plist, dest, start);
		
	}
	
	@Override
	public boolean start() {
		
		try {
			Files.walkFileTree(this.dest, new DeletingWalker(this.dest));
		} catch (IOException e1) {
			Console.error(String.format("Delete Destination %s failed",this.dest.toAbsolutePath().toString()));
		}
		
		try {
			Files.walkFileTree(start, simpleCodeWalker);
		} catch (IOException e) {
			Console.error("Failed walk through source files", e);
			return false;
		}
		
		return true;
	}

	@Override
	public void stop() {
		// nothing to do
		
	}
	
	
	
	
}
