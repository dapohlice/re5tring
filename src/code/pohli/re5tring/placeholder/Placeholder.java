package code.pohli.re5tring.placeholder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

/***
 * Placeholder for Replacing Strings in Sourcecode
 * @author dapohlice
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class Placeholder {
	@XmlAttribute(name="name")
	protected String fullName;
	@XmlTransient
	private String name;
	
	/***
	 * Standard Constructor name = ""
	 */
	public Placeholder() {
		this.name = "";
		this.fullName = "";
	}
	
	/***
	 * Standard Constructor set name
	 * @param name Placeholdername
	 */
	public Placeholder(String fullName) {
		setFullName(fullName);
		
	}

	/***
	 * get Placeholder.FullName
	 * Format "directory/../../name"
	 * @return name
	 */
	public String getFullName() {
		return fullName;
	}

	/***
	 * set Placeholder.FullName
	 * @param name
	 */
	public void setFullName(String name) {
		setFullName(name.split("/"));
		
	}
	
	/***
	 * set Placeholder.FullName as Array
	 * @param name
	 */
	public void setFullName(String[] nameArray) {
		if(nameArray.length > 0)
		{
			nameArray[nameArray.length-1] = Placeholder.deleteType(nameArray[nameArray.length-1]); 
			this.fullName = String.join("/", nameArray);
			this.name = nameArray[nameArray.length-1];
		}else {
			setFullName(nameArray[0]);
		}
	}
	
	/***
	 * Generates List of Directories
	 * @return List of Directories
	 */
	public String[] getNameList() {
		return this.fullName.split("/");
	}
	
	/***
	 * Get Placeholder Simple Name
	 * @return Name
	 */
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = Placeholder.deleteType(name);
		String[] sl = getNameList();
		
		if(sl.length > 0)
		{
			sl[sl.length-1] = this.name ;
			this.fullName = String.join("/", sl);
		}else {
			setFullName(this.name);
		}
	}
	
	protected static String deleteType(String name) {
		try {
			String[] a = name.split("\\.");
			return a[0];
		}catch(ArrayIndexOutOfBoundsException ex)
		{
			ex.printStackTrace();
		}
		return "";
	}
	

	@Override
	public String toString() {
		return "Placeholder [fullName=" + fullName + ", name=" + name + "]";
	}
	
	
	public abstract String getValue();
	
	
}
