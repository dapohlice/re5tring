package code.pohli.re5tring.placeholder;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

import code.pohli.utils.console.Console;
import code.pohli.re5tring.placeholder.variables.PVarList;
import code.pohli.re5tring.settings.General;

public class PList {
	
	private HashMap<String, Placeholder> placeholder;
	private PVarList variables;
	
	private Semaphore canAdd;
	
	private Path placeholderPath;

	public PList(Path placeholderPath) {
		super();
		
		this.placeholderPath = placeholderPath; 
		
		this.placeholder = new HashMap<String, Placeholder>();
		this.variables = new PVarList();	
		
		this.canAdd = new Semaphore(1);
		
		
	}
	
	public void load(boolean preload)
	{
		
		this.variables = PVarList.load(General.getSettings().getWorkingDir());
		
		try {
			Files.walkFileTree(placeholderPath, new PlaceholderWalker(this.placeholder,placeholderPath,preload));
			
		} catch (IOException e) {
			Console.error("Directorys not found "+placeholderPath.toAbsolutePath(), e);
		}	
		
	}
	
	/**
	 * search placeholder by Name(Path)
	 * 
	 * @param name
	 * @return placeholder or null when not found
	 */
	
	public String getPlaceholder(String name)
	{
		try {
			this.canAdd.acquire();
		} catch (InterruptedException e) {
			Console.error("Unknown Interrupt in PList", e);
		}
		
		Placeholder ret = this.placeholder.get(name);

		String result;
		
		if(ret == null) {
			result = null;
		}else {
			result = ret.getValue();	
		}
		
		this.canAdd.release();
		
		return result; 
	}
	
	
	
	public String getVariable(String name)
	{
		return this.variables.getValue(name);
	}
	
	
	/***
	 * Create or changes a placeholder
	 * @param path
	 */
	public void setPlaceholder(Placeholder placeholder)
	{
		try {
			this.canAdd.acquire();
		} catch (InterruptedException e) {
			Console.error("Unknown Interrupt in PList", e);
		}
		
		if(this.placeholder.containsKey(placeholder.getFullName())) {
			this.placeholder.remove(placeholder.getFullName());
		} 
			
		this.placeholder.put(placeholder.getFullName(),placeholder);
		
		
		
		
		this.canAdd.release();
	}
	
	
	public void removePlaceholder(String name)
	{
		try {
			this.canAdd.acquire();
		} catch (InterruptedException e) {
			Console.error("Unknown Interrupt in PList", e);
		}
		
		this.placeholder.remove(name);
		
		this.canAdd.release();
	}
	
	
	
}
