package code.pohli.re5tring.placeholder;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

import code.pohli.utils.console.Console;
import code.pohli.re5tring.settings.General;

public class PText extends Placeholder{

	private String value;
	private String fileName;
	
	public PText() {
		this("");
	}
	
	public PText(String fullName) {
		super(fullName);
		this.fileName = fullName;
		value = null;
	}
	

	/***
	 * Loads Text-Placeholder from File
	 * @return Load Success
	 */
	public boolean load() {
		Path path = General.getSettings().getPlaceholderPath().resolve(this.fileName);
		
		//System.out.println("load "+path+":"+this.getFullName());
		
	  try(BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"))){

	      StringBuilder sb = new StringBuilder();
	      String currentLine = null;
	      while((currentLine = reader.readLine()) != null){//while there is content on the current line
	    	  sb.append(currentLine).append(General.getLineBreak());
	      }
	      
	      this.value = sb.toString();
	      	      
	    }catch(IOException ex){
	      Console.error(path.toString() +" not found", ex);
	      
	      value = "";
	      return false;
	    }
	  
	  return true;
		
	}

	@Override
	public String getValue() {
		if(this.value == null)
		{
			this.load();
		}
		
		return this.value;
	}

	@Override
	public String toString() {
		return "PText [value=" + value + ", getFullName()=" + getFullName() + ", getName()=" + getName() + "]";
	}
	
	
	
	

	
}
