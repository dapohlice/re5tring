package code.pohli.re5tring.placeholder;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Map;

public class PlaceholderWalker extends SimpleFileVisitor<Path> {
	
	private Map<String,Placeholder> map;
	private Path startPath;
	private boolean preload;
	
	public PlaceholderWalker(Map<String,Placeholder> map,Path startPath,boolean preload) {
		this.map = map;
		this.startPath = startPath;
		this.preload = preload;
	}
	
	@Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
        throws IOException
    {
       
			PText p = new PText(startPath.relativize(file).toString());
			
			
			map.put(p.getFullName(),p);
			if(this.preload)
			{
				p.load();
			}
			
			
			//System.out.println(p);
		
        return FileVisitResult.CONTINUE;
    }
}
