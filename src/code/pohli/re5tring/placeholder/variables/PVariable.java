package code.pohli.re5tring.placeholder.variables;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import code.pohli.re5tring.placeholder.Placeholder;


@XmlRootElement(name = "var")
@XmlAccessorType(XmlAccessType.FIELD)

public class PVariable extends Placeholder implements IVariable{

	@XmlAttribute(name="value")
	private String var;
	
	public PVariable() {
		// TODO Auto-generated constructor stub
	}

	public PVariable(String fullName) {
		super(fullName);
		// TODO Auto-generated constructor stub
	}
	
	public String getVar()
	{
		return var;
	}
	
	public void setVar(String var)
	{
		this.var = var;
	}
	

	@Override
	public String getValue() {
		return var;
	}

	@Override
	public void setValue(Object o) {
		if(o instanceof String)
		{
			this.var = (String)o;
		}
		
		
	}

	@Override
	public Object getObject() {
		return var;
	}

		
	

}
