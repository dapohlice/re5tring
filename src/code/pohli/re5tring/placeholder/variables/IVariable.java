package code.pohli.re5tring.placeholder.variables;

public interface IVariable {
	public String getValue();
	public void setValue(Object o);
	public Object getObject();
}
