package code.pohli.re5tring.placeholder.variables;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import code.pohli.utils.console.Console;


public class PVarList {

	private static final String FILE_NAME = "variables.rv5";
	
	@XmlRootElement(name = "varList")
	public static class PVarListSaveData{
		
		private List<PVariable> list;
		
		private PVarListSaveData() {
			this.list = new ArrayList<PVariable>();
		}
		
		
		@XmlElement(name="var")
		public List<PVariable> getList() {
			return this.list;
		}
		
		public void setList(List<PVariable> list) {
			this.list = list;
		}
		
		
	}
	
	
	
	
	private HashMap<String,PVariable> map;
	
	
	public PVarList() {
		map = new HashMap<String, PVariable>();
		
	}

	public List<PVariable> getList() {
		return new ArrayList<PVariable>(this.map.values());
	}

	public void setList(List<PVariable> list) {
		map = new HashMap<String, PVariable>();

		for (PVariable var : list) {
			if(!this.addVariable(var))
			{
				Console.warn(String.format("Duplicate detected: %s, removed Variable %s", var.getFullName(),var.getValue()));
			}
		}
		
	}
	
	
	
	public boolean addVariable(PVariable var)
	{
		String name = var.getFullName();
		
		if(this.map.containsKey(name))
		{
			return false;
		}
		
		map.put(name, var);
		return true;
		
	}
	
	
	public void removeVariable(String name)
	{
		map.remove(name);
		
	}
	
	
	public String getValue(String name)
	{
		
		PVariable var = map.get(name);
		if(var == null)
		{
			return null;
		}
		return var.getValue();
	}
	
	public static PVarList load(String path)
	{
		PVarList list  = new PVarList();;
		try {
			JAXBContext context = JAXBContext.newInstance(PVarListSaveData.class);
			Unmarshaller um = context.createUnmarshaller();
			PVarListSaveData data = (PVarListSaveData)um.unmarshal(new File(path+File.separator+PVarList.FILE_NAME));
			
			list = new PVarList();
			list.setList(data.getList());
			
		}catch(Exception e)
		{
			Console.warn("Failed loading Variables");
			
		}
		
		return list;
	}
	
	public static boolean fileExists(String path)
	{
		return Files.exists(Paths.get(path,FILE_NAME));
	}
	
	public boolean save(String path)
	{
		try {
			
			PVarListSaveData data = new PVarListSaveData();
			data.setList(this.getList());
			
			JAXBContext context = JAXBContext.newInstance(PVarListSaveData.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
			m.marshal(data, new File(path+File.separator+PVarList.FILE_NAME));	
		}catch(Exception e)
		{
			Console.error("Failed saving Variables",e);
			return false;
		}
		return true;
		
	}
	
	

}
