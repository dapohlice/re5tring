package code.pohli.re5tring.placeholder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import code.pohli.fileObserver.IFileChanger;
import code.pohli.re5tring.re5tring.SimpleRe5tring;
import code.pohli.re5tring.walker.SimpleCodeWalker;
import code.pohli.utils.console.Console;


public class PlaceholderObserver implements IFileChanger {

	private boolean preload;
	private Path startPath;
	
	private PList placeholderList;
	
	private SimpleCodeWalker codeWalker;
	
	public PlaceholderObserver(PList plist,SimpleCodeWalker simpleCodeWalker,Path startPath, boolean preload)
	{
		this.placeholderList = plist;
		this.startPath = startPath;
		this.preload = preload;
		this.codeWalker = simpleCodeWalker;
	}
	
	@Override
	public void deleteFile(Path file) {
		Console.log("Placeholder deleted: "+file.toString());
		placeholderList.removePlaceholder(startPath.relativize(file).toString());
		repar5eSource();

	}

	@Override
	public void createFile(Path file) {
		Console.log("Placeholder added: "+file.toString());
		addPlaceholder(file);
		repar5eSource();
	}

	@Override
	public void modifyFile(Path file) {
		Console.log("Placeholder changed: "+file.toString());
		addPlaceholder(file);
		repar5eSource();
	}

	@Override
	public void deleteDir(Path dir) {
		// TODO Auto-generated method stub

	}

	@Override
	public void createDir(Path dir) {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifyDir(Path dir) {
		// TODO Auto-generated method stub

	}

	
	private void addPlaceholder(Path path)
	{
		PText p = new PText(startPath.relativize(path).toString());
		
		if(this.preload)
		{
			p.load();
		}
		
		this.placeholderList.setPlaceholder(p);
	}
	
	
	private void repar5eSource() {
		Console.log("Repar5e Source");
		try {
			Files.walkFileTree(codeWalker.getStartPath(), codeWalker);
		} catch (IOException e) {
			Console.error("Failed walk through source files", e);
		}
	}
	

}
