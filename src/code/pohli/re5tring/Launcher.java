package code.pohli.re5tring;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import code.pohli.utils.console.Console;
import code.pohli.re5tring.re5tring.SimpleRe5tring;
import code.pohli.re5tring.re5tring.WatchingRe5tring;
import code.pohli.re5tring.settings.General;
import code.pohli.re5tring.settings.Settings;

public class Launcher {

	public static void main(String[] args) {
		
		String source = null;
		String destination = null;
		String placeholder = null;
		String workingDir = General.getPWD();
		
		boolean once = false;
		boolean debug = false;
		
		System.out.println(workingDir);
		
		
		
		for(int i = 0; i < args.length; i++)
		{
			
			switch(args[i])
			{
				case "--debug":
					System.out.println("Debug Mode");
					Console.console.setDebug(true);
					debug = true;
					
					break;
				case "-o":
				case "--once":
					once = true;
					break;
				case "-q":
				case "--quiet":
					Console.console.setVerbose(false);
					break;
			
				case "-d":
				case "--destination":
					if(args.length <= ++i)
					{
						Console.warn("Missing Destination Path");
						System.exit(10);
					}
					
					destination = args[i];
					break;
				case "-src":
				case "--source":
					if(args.length <= ++i)
					{
						Console.warn("Missing Source Path");
						System.exit(11);
					}
					source = args[i];
					break;
				case "-p":
				case "--placeholder":
					if(args.length <= ++i)
					{
						Console.warn("Missing Placeholder Path");
						System.exit(12);
					}
					
					placeholder = args[i];
					break;
				case "-w":
				case "--workingdir":
					if(args.length <= ++i)
					{
						Console.warn("Missing WorkingDirectory Path");
						System.exit(13);
					}
					
					workingDir = args[i];
					break;
			}
			
		}
		
		General.initialise(workingDir);
		
		Settings s = General.getSettings();
		
		if(workingDir != null)
		{
			s.setWorkingDir(workingDir);
			s.setUseWorkingDir(true);
		}
		
		if(source != null)
		{
			s.setSourceDirectory(source);
		}
		
		if(destination != null)
		{
			s.setDestinationDirectory(destination);
		}
		
		if(placeholder != null)
		{
			s.setPlaceholderDirectory(placeholder);
		}
		
		if(!Files.exists(s.getPlaceholderPath()))
		{
			Console.error("Placeholder Path "+s.getPlaceholderDirectory()+" not found");
			System.exit(4);
		}
		
		
		if(!Files.exists(s.getDestinationPath()))
		{
			Console.error("Destination Path "+s.getDestinationDirectory() + " not found");
			System.exit(5);
		}
		
		if(!Files.exists(s.getSourcePath()))
		{
			Console.error("Source Path "+s.getSourceDirectory()+" not found");
			System.exit(6);
		}

		
		
		
		if(once)
		{
			SimpleRe5tring sr5s = new SimpleRe5tring();
			if(!sr5s.start())
			{
				System.exit(1);
				
			}
		}else {
			WatchingRe5tring wr5s = new WatchingRe5tring();
			if(!wr5s.start())
			{
				System.exit(1);
			}
			
			
			
			Runtime.getRuntime().addShutdownHook(new Thread() {
		        public void run() {

		                System.out.println(System.lineSeparator()+"Shouting down ...");
		                
		                wr5s.stop();
		                
		                System.out.println("re5tring end");
		        }
		    });
			
			
			
			if(debug)
			{
				//TODO: NullPointer Exception 
				//Console.console.setLog(Paths.get(General.getPWD(),"log.log"));
				
			/***
			 * 
			 * for Eclipse
			 */
			try{
				Scanner scn = new Scanner(System.in);
		        while (true)
		        {
		        	try {
						if(System.in.available()!=0){
							System.out.println("end");
							System.exit(0);
						}
					} catch (IOException e) {
					}
		        	
		        	
		            Thread.sleep(200);
		        }
		        
			 }catch(InterruptedException e)
			 {
				 System.out.println("Interupt");
				 
			 }
			
			}else{
			/***
			 * 
			 * for normal
			 * 
			 * 
			 */
			try{
		        while (true)
		        {
		        	
		            Thread.sleep(1000);
		        }
			 }catch(InterruptedException e)
			 {
				 System.out.println("Interupt");
			 }
			}
		}
		
		

	}

}
