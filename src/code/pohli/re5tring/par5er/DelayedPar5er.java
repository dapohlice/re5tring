package code.pohli.re5tring.par5er;

import code.pohli.utils.console.Console;

import java.nio.file.Path;

import code.pohli.fileObserver.IFileChanger;


public class DelayedPar5er implements IFileChanger {

	FilePar5er par5er;
	
	public DelayedPar5er(FilePar5er par5er)
	{
		this.par5er = par5er;
	}
	
	@Override
	public void deleteFile(Path file) {
		
		par5er.deleteDestinationFile(file);

	}

	@Override
	public void createFile(Path file) {
		
		par5er.parse(file);

	}

	@Override
	public void modifyFile(Path file) {
		
		par5er.parse(file);

	}

	@Override
	public void deleteDir(Path dir) {
		
		par5er.deleteDestinationDirectory(dir);

	}

	@Override
	public void createDir(Path dir) {
		
		par5er.createDestinationDirectory(dir);

	}

	@Override
	public void modifyDir(Path dir) {
		
		Console.debug("Modify Dir nothing to do: "+dir.toString());

	}

}
