package code.pohli.re5tring.par5er;

public class Par5er {
	
	public enum Par5erReturn{
		NOT,
		PARSE,
		RESET,
		FINISHED		
	}
	
	
	
	private enum Par5erState{
		FIRST,
		SECOUND,
		UNDERLINE,
		TYPE,
		PATH,
		LAST,
		END
	}
	
	private Par5erState State;
	private StringBuilder type;
	private StringBuilder path;
	
	private StringBuilder text;
	
	private int offset;
	
	private boolean finish; 
	
	public Par5er() {
		reset();
		this.text = new StringBuilder();
	}	
	
	private void reset() {
		this.finish = false;
		this.type = new StringBuilder();
		this.path = new StringBuilder();
		
		
		
		this.State = Par5erState.FIRST;
		this.offset = 0;
	}
	
	
	
	/***
	 * Parses $$_type:path$$
	 * @param c nextCharacter
	 * @return parsing Line finished
	 */
	public Par5erReturn parse(char c)
	{
		this.text.append(c);
		switch(this.State)
		{
			case FIRST:
				if(c == '$')
				{
					this.State = Par5erState.SECOUND;
					this.offset++; 
					return Par5erReturn.PARSE;
				}else {
					return Par5erReturn.NOT;
				}
				
				
			case SECOUND:
				if(c == '$')
				{
					this.State = Par5erState.UNDERLINE;
					this.offset++;
					return Par5erReturn.PARSE;
				}else {
					reset();
				}
				break;
				
			case UNDERLINE:
				if(c == '_')
				{
					this.State = Par5erState.TYPE;
					this.offset++;
					return Par5erReturn.PARSE;
				}else {
					reset();
				}
				break;
				
			case TYPE:
				if(c == ':')
				{
					this.State = Par5erState.PATH;
					this.offset++;
					return Par5erReturn.PARSE;
				}else {
					if(Character.isUpperCase(c) || Character.isLowerCase(c))
					{
						this.type.append(c);
						this.offset++;
						return Par5erReturn.PARSE;
					}else {
						reset();
					}
				}
				break;
				
			case PATH:
				if(c == '$')
				{
					this.State = Par5erState.LAST;
					this.offset++;
					return Par5erReturn.PARSE;
				}else {
					if(Character.isUpperCase(c) || Character.isLowerCase(c) || c == '/')
					{
						this.path.append(c);
						this.offset++;
						return Par5erReturn.PARSE;
					}else {
						reset();
					}
				}
				break;
				
			case LAST:
				if(c == '$')
				{
					this.State = Par5erState.END;
					this.offset++;
					this.finish = true;
					return Par5erReturn.FINISHED;
				}else {
					reset();
				}
				break;
				
			case END:
				if(c == '$')
				{
					reset();
					this.State = Par5erState.SECOUND;
					this.offset++;
					return Par5erReturn.PARSE;
				}else {
					return Par5erReturn.NOT;
				}
				
		}
		return Par5erReturn.RESET;
	}
	
	
	
	public boolean isFinished() {
		return this.finish;
	}
	
	public String getType() {
		return this.type.toString();
	}
	
	public String getPath() {
		return this.path.toString();
	}
	
	public int getOffset() {
		return this.offset;
	}
	
	
	public String getText() {
		String ret = this.text.toString();
		this.text = new StringBuilder();
		return ret;
	}
	
	
	public static void parseFile(){
		
	}
	
	
}
