package code.pohli.re5tring.par5er;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import code.pohli.utils.console.Console;
import code.pohli.re5tring.placeholder.PList;
import code.pohli.re5tring.settings.General;

public class FilePar5er {

	private PList placeholder;
	private Path destination;
	private Path start;
	
	public FilePar5er(PList placeholder, Path destination, Path start) {
		super();
		this.placeholder = placeholder;
		this.destination = destination;
		this.start = start;
	}
	
	public boolean deleteDestinationFile(Path filePath)
	{
		Path destPath = this.destination.resolve(start.relativize(filePath));
		
		if(Files.exists(destPath)){
			try {
				Files.delete(destPath);
				return true;
			} catch (IOException e) {
				Console.error("Failed deleting File "+filePath.toString(),e);
			}
		}
		return false;
	}
	
	
	public boolean createDestinationDirectory(Path filePath)
	{
		Path destPath = this.destination.resolve(start.relativize(filePath));
		
		Console.debug("Create Destination Dir "+filePath);
		if(!Files.exists(destPath))
		{
			try {
				Files.createDirectories(destPath);
			} catch (IOException e) {
				Console.error("Failed to create Destination Directory "+destPath);
				return false;
			}
		}
		return true;
	}

	public boolean deleteDestinationDirectory(Path filePath)
	{
		Path destPath = this.destination.resolve(start.relativize(filePath));
		
		Console.debug("Delete Destination Dir "+filePath);
		if(Files.exists(destPath))
		{
			try {
			
				Files.delete(destPath);				
			    
			} catch (IOException e) {
				Console.error("Failed to delete Destination Directory "+destPath,e);
				return false;
			}
		}
		return true;
	}
	
	
	public boolean parse(Path filePath)
	{
		try {
			if(Files.isHidden(filePath))
			{
				Console.log(String.format("File %s is hidden",filePath));
				return false;
			}
		} catch (IOException e1) {
			Console.error("Coundn't read "+ filePath);
			return false;
		}
		
		Par5er parser = new Par5er();
	
		if(!Files.exists(filePath))
		{
			Console.log(filePath.toAbsolutePath().toString() + " does not exist");
			return false;
			
		}
		Console.log("Parse: "+filePath.toAbsolutePath().toString());
		
		Path destPath = this.destination.resolve(start.relativize(filePath));
		
		
		try{
			
			
			BufferedReader in = Files.newBufferedReader(filePath, Charset.forName("UTF-8"));
			OutputStream out = Files.newOutputStream(
					destPath,
					StandardOpenOption.CREATE,
					StandardOpenOption.WRITE,
					StandardOpenOption.TRUNCATE_EXISTING);
			
			StringBuilder outputSb = new StringBuilder();
			
		
			int i = in.read(); 
			while(i >-1) {
				char c = (char)i;
				
				switch(parser.parse(c)) {
					case NOT:
							outputSb.append(c);
						break;
					case FINISHED:

						
							String path = parser.getPath();
							
							String insert = "";
							
							switch(parser.getType()) {
								case "text":
									insert = this.placeholder.getPlaceholder(path);	
									break;
								case "var":
									insert = this.placeholder.getVariable(path);
									
							
							}
							
							if(insert == null)
							{
								Console.log(
										String.format(
												"placeholder (%s) not found%n    name: %s%n    file: %s",
												parser.getType(),
												path,
												filePath.toString()
												)
										);
								insert =  General.getSettings().getErrorPlaceholder();
								
							}
							
							
							outputSb.append(insert);
						break;
					case RESET:
							String text = parser.getText();
							outputSb.append(text);
						break;
						
					case PARSE:
						
						break;
					
				}
				i = in.read();
			}
			
			out.write(outputSb.toString().getBytes(Charset.forName("UTF-8")));
			
			out.flush();
			
			out.close();
			
			in.close();
			
			return true;
		}catch(Exception e)
		{
			Console.error("Failed parse File: "+filePath.toAbsolutePath().toString(),e);
			e.printStackTrace();
			
			Console.log(filePath.toAbsolutePath().toString()+" is Binary File");
			try {
				//create FileInputStream object for source file
				FileInputStream fin = new FileInputStream(filePath.toString());
				
				//create FileOutputStream object for destination file
				FileOutputStream fout = new FileOutputStream(destPath.toString());
				
				byte[] b = new byte[1024];
				int noOfBytes = 0;
				
				Console.debug("Copying file using streams");
				
				//read bytes from source file and write to destination file
				while( (noOfBytes = fin.read(b)) != -1 )
				{
					fout.write(b, 0, noOfBytes);
				}
				
				Console.debug("File copied!");
				
				//close the streams
				fin.close();
				fout.close();			
				
			}catch(Exception ex)
			{
				Console.error("Failed copy binary File: "+filePath.toAbsolutePath().toString(),ex);
				ex.printStackTrace();
			}

			
			
		}
		
		
		return false;
	}

}
