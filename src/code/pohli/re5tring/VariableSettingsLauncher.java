package code.pohli.re5tring;

import code.pohli.utils.console.Console;
import code.pohli.re5tring.placeholder.variables.PVarList;
import code.pohli.re5tring.placeholder.variables.PVariable;
import code.pohli.re5tring.settings.General;
import code.pohli.re5tring.settings.Settings;

public class VariableSettingsLauncher {

	private enum VarStatus{
		CHANGE,
		ADD,
		REMOVE,
		NONE
	}
	
	
	
	public static void main(String[] args) {
		
		if(args.length == 1)
		{
			if(args[0].equals("-h") || args[0].equals("--help") || args[0].equals("help"))
			{
				System.out.printf(
							"Variablemanger (re5tring) - HELP%n"
						+ "————————————————————————————————————————————————————%n"
						+ "General%n"
						+ "———————%n"
						+ "help | -h |--help%n"
						+ "\t shows this screen%n%n"
						+ "-s | --settings%n"
						+ "\t sets the path of the settingsfile%n%n"
						+ "-w | --workingsdir%n"
						+ "\t sets the path of the workingsdir%n%n"
						+ "Variablemanger%n"
						+ "——————————————%n"
						
						+ "add <name> <value>%n"
						+ "\t adds a new variable, if <name> not alreay exists%n%n"
						+ "change <name> <value>%n"
						+ "\t changes the value from the variable%n%n"
						+ "remove <name>%n"
						+ "\t removes the variable%n%n"
						
						
						);
				return;
			}
		}
		
		
		String settings = General.getPWD();
		String workingDir = null;
		
		String name = "";
		String var = "";
		
		VarStatus status = VarStatus.NONE; 
		
		
		General.initialise();

		
		for(int i = 0; i < args.length; i++)
		{
			
			
			
			switch(args[i])
			{	
				case "-s":
				case "--settings":
					if(args.length < ++i)
					{
						Console.warn("Missing Setting Path");
						System.exit(12);
					}
					settings = args[i];
					break;
				
				case "-w":
				case "--workingdir":
					if(args.length <= ++i)
					{
						Console.warn("Missing Placeholder Path");
						System.exit(13);
					}
					
					workingDir = args[i];
					break;
					
				case "add":
					System.out.println(i+"add" + args.length );
					if(args.length > i+2)
					{
						System.out.println("add");
						
						name = args[i+1];
						var = args[i+2];
						i+=2;
						status = VarStatus.ADD;
					}else {
						System.out.println("Wrong Parameters: add <name> <value>");
					}
					break;
				case "change":
					if(args.length > i+2)
					{
						name = args[i+1];
						var = args[i+2];
						i+=2;
						status = VarStatus.CHANGE;
					}else {
						System.out.println("Wrong Parameters: change <name> <value>");
					}
					break;
				case "remove":
					if(args.length > ++i)
					{
						name = args[i];
						status = VarStatus.REMOVE;
					}else {
						System.out.println("Wrong Parameter: remove <name>");
					}
					break;
			}
		}
		
		if(status == VarStatus.NONE)
		{
			System.out.println("Wrong command, see help");
			System.exit(19);
		}
		
		
		General.initialise(settings);
		
		Settings s = General.getSettings();
		
		if(workingDir != null)
		{
			s.setWorkingDir(workingDir);
			s.setUseWorkingDir(true);
		}
		
		PVarList list = PVarList.load(s.getWorkingDir());
		if(list == null)
		{
			Console.log("Create new Variable File");
			list = new PVarList();
		}
		
		switch(status)
		{
			
			case CHANGE:
				list.removeVariable(name);
		
			case ADD:
				PVariable v = new PVariable(name);
				v.setVar(var);
				if(list.addVariable(v))
				{
					System.out.println("Added "+name);
				}else {
					System.out.println(name+" already exists.");
					System.exit(20);
				}
				break;
				
			case REMOVE:
				list.removeVariable(name);
				break;
			
		
		}
		
		list.save(s.getWorkingDir());
		
		
		
		
		
	}

}
