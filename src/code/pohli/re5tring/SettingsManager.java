package code.pohli.re5tring;

import code.pohli.utils.console.Console;
import code.pohli.re5tring.settings.Settings;

public class SettingsManager {
	
	public static void main(String[] args) {
		
		String working = null;
		String placeholder = Settings.DEFAULT_PLACEHOLDER_DIR;
		String source = Settings.DEFAULT_SOURCE_DIR;
		String destination = Settings.DEFAULT_DESTINATION_DIR;
		String error = Settings.DEFAULT_ERROR;
		
		Settings settings;
		
		for(int i = 0; i < args.length; i++)
		{
			
			switch(args[i])
			{
				case "--debug":
					System.out.println("Debug Mode");
					Console.console.setDebug(true);
					break;
				case "-q":
				case "--quiet":
					Console.console.setVerbose(false);
					break;
			
				case "-d":
				case "--destination":
					if(args.length <= ++i)
					{
						Console.warn("Missing Destination Path");
						System.exit(10);
					}
					
					destination = args[i];
					break;
				case "-src":
				case "--source":
					if(args.length <= ++i)
					{
						Console.warn("Missing Source Path");
						System.exit(11);
					}
					source = args[i];
					break;
				case "-p":
				case "--placeholder":
					if(args.length <= ++i)
					{
						Console.warn("Missing Placeholder Path");
						System.exit(13);
					}
					
					placeholder = args[i];
					break;
				case "-w":
				case "--workingdir":
					if(args.length <= ++i)
					{
						Console.warn("Missing WorkingDirectory Path");
						System.exit(13);
					}
					
					working = args[i];
					break;
				case "-e":
				case "--error":
					if(args.length <= ++i)
					{
						Console.warn("Missing Error Message");
						System.exit(14);
					}
					
					error = args[i];
					break;
				
			}
			
		}
		
		if(working == null)
		{
			Console.error("Working Directory must be given.");
			System.exit(2);
		}
		
		settings = new Settings();
		settings.setDestinationDirectory(destination);
		settings.setPlaceholderDirectory(placeholder);
		settings.setSourceDirectory(source);
		
		settings.setErrorPlaceholder(error);
		
		if(!settings.createProjectDirectory(working)) {
			System.exit(1);
		}
		
		Console.log("Succesfully created Project Directory");
		
		
	}
}
