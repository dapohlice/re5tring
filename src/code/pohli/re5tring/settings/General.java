package code.pohli.re5tring.settings;

import java.nio.file.Paths;

public class General {

	private static Settings settings;
	private static String pwd =  Paths.get("").toAbsolutePath().toString();
	
	public static Settings getSettings() {
		
		return settings;
	}
	
	public static String getLineBreak() {
		
		return settings.getLineBreak();
	}
	
	public static String getPWD() {
		return pwd;
	}
	
	/**
	 * Initialize Settings
	 * @param path Path to Settings
	 */
	public static void initialise(String path) {
		General.settings = Settings.loadSettings(path);
	}
	
	/**
	 * Initialize Settings from PWD
	 */
	public static void initialise() {
		
		General.settings = Settings.loadSettings(pwd);
	}
	
}
