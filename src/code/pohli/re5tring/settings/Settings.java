package code.pohli.re5tring.settings;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

import code.pohli.utils.console.Console;

@XmlRootElement(name = "re5tringSettings")
public class Settings {
	
	public static final String FILE = "project.r5";
	
	
	public static final String DEFAULT_PLACEHOLDER_DIR = "placeholder";
	public static final String DEFAULT_DESTINATION_DIR = "destination";
	public static final String DEFAULT_SOURCE_DIR = "source";
	
	public static final String DEFAULT_ERROR = "ERROR";
	public static final String DEFAULT_LINEBREAK = "\n";
	
	
	/***
	 * Project Directory
	 */
	private String workingDir = "project";
	private boolean useWorkingDir = true;
	
	
	private String lineBreak = DEFAULT_LINEBREAK;
	
	private String placeholderDirectory = DEFAULT_PLACEHOLDER_DIR;
	private String destinationDirectory = DEFAULT_DESTINATION_DIR;
	private String sourceDirectory = DEFAULT_SOURCE_DIR;
	
	private String errorPlaceholder = DEFAULT_ERROR;
	
	private String getCurrentWorkingDir() {
		if(useWorkingDir)
		{
			return getWorkingDir();
		}
		
		return General.getPWD();
		
	}
	
	
	public boolean createProjectDirectory(String projectDir) {
		
		Path destination = Paths.get(projectDir);
		
		try {
			Files.createDirectories(destination.resolve(Paths.get(this.getPlaceholderDirectory())));
			Path dest = destination.resolve(Paths.get(this.getDestinationDirectory()));
			Files.createDirectories(dest);
		
			Files.createDirectories(destination.resolve(Paths.get(this.getSourceDirectory())));
		} catch (IOException e) {
			Console.error("Failed creating Project Directory", e);
			return false;
		}
		
		if(!this.save(destination)) {
			Console.error("Failed creating Settings");
			return false;
		}
		
		return true;
		
	}
	
	
	public String getWorkingDir() {
		return workingDir;
	}

	public void setWorkingDir(String workingDir) {
		this.workingDir = workingDir;
	} 
	
	public boolean isUseWorkingDir() {
		return useWorkingDir;
	}

	public void setUseWorkingDir(boolean useWorkingDir) {
		this.useWorkingDir = useWorkingDir;
	}

	
	public String getLineBreak() {
		return lineBreak;
	}

	public void setLineBreak(String lineBreak) {
		this.lineBreak = lineBreak;
	}
	
	
	public Path getDestinationPath() {
		return Paths.get(getCurrentWorkingDir(),getDestinationDirectory());
	}

	public String getDestinationDirectory() {
		return destinationDirectory;
	}

	public void setDestinationDirectory(String destinationDirectory) {
		this.destinationDirectory = destinationDirectory;
	}
	

	public Path getSourcePath() {
		return Paths.get(getCurrentWorkingDir(),getSourceDirectory());
	}

	public String getSourceDirectory() {
		return sourceDirectory;
	}

	public void setSourceDirectory(String sourceDirectory) {
		this.sourceDirectory = sourceDirectory;
	}

	
	
	public String getErrorPlaceholder() {
		return errorPlaceholder;
	}

	public void setErrorPlaceholder(String errorPlaceholder) {
		this.errorPlaceholder = errorPlaceholder;
	}

	
	public Path getPlaceholderPath() {
		return Paths.get(getCurrentWorkingDir(),getPlaceholderDirectory());
	}
	

	public String getPlaceholderDirectory() {
		return placeholderDirectory;
	}

	public void setPlaceholderDirectory(String placeholderDirectory) {
		this.placeholderDirectory = placeholderDirectory;
	}

	
	public boolean save(Path path)
	{
		String dest = path.toString();
		return save(dest);
	}
	
	public boolean save(String path) {
		try {
			JAXBContext context = JAXBContext.newInstance(Settings.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
			File f =  new File(path,Settings.FILE);
			m.marshal(this,f);	
		}catch(Exception ex)
		{
			Console.error("Can not save settings.",ex);
			return false;
		}
		return true;
	}
	
	/**
	 * load Settings from XML file
	 * @param path Directory to Settings
	 * @return Settings from File or Standard Settings
	 */
	
	public static Settings loadSettings(String path){
		/*Settings s = new Settings();
		s.setWorkingDir("project");
		s.setLineBreak(System.lineSeparator());
		s.setPlaceholderDirectory("placeholder");
		s.setErrorPlaceholder("ERROR");
		s.setDestinationDirectory("destination");
		s.setSourceDirectory("source");*/
		
		
		try {
			JAXBContext context = JAXBContext.newInstance(Settings.class);
			Unmarshaller unm = context.createUnmarshaller();
			Settings s = (Settings)unm.unmarshal(new File(path,Settings.FILE));
			return s;
		} catch (JAXBException e) {
			Console.error(String.format("Can not load Settings from: %s/%s",path,Settings.FILE),e);	
		}
		return new Settings();
	}
	
	
	public static boolean settingsExists(String path)
	{
		return Files.exists(Paths.get(path, FILE));
	}
	
}
