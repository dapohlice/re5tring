package code.pohli.re5tring.walker;


import java.io.IOException;

import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import code.pohli.re5tring.par5er.FilePar5er;
import code.pohli.re5tring.placeholder.PList;


public class SimpleCodeWalker extends SimpleFileVisitor<Path>{
	
	private Path start;
	private FilePar5er fileParser;
	
	public SimpleCodeWalker(PList placeholder, Path destination, Path start) {
		super();
		this.fileParser = new FilePar5er(placeholder, destination, start);
		this.start = start;
	}


	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		
		this.fileParser.createDestinationDirectory(dir);
		
		return super.preVisitDirectory(dir, attrs);
	}

	@Override
	public FileVisitResult visitFile(Path arg0, BasicFileAttributes arg1) throws IOException {
		
		this.fileParser.parse(arg0);
		
		return FileVisitResult.CONTINUE;
	}
	
	public Path getStartPath() {
		return start;
	}
	
}
