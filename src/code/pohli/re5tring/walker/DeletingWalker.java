package code.pohli.re5tring.walker;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import code.pohli.utils.console.Console;

public class DeletingWalker extends SimpleFileVisitor<Path> {

	
	private Path homeDir;
	
	public DeletingWalker(Path homeDir) {
		this.homeDir = homeDir;
	}
	
	
	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		
		if(!dir.equals(this.homeDir))
		{
			Console.debug("Delete Directory "+dir);
			Files.delete(dir);
		}
		
		
		return super.postVisitDirectory(dir, exc);
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		Console.debug("Delete File " +file);
		Files.delete(file);
		
		return super.visitFile(file, attrs);
	}

}
